cmake_minimum_required(VERSION 3.8.2)

project(qt-hello)

# Add a compiler flag
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

# Find the QtCore library
find_package(Qt5Core)

# Variables
set(TARGET_BASE_NAME "${PROJECT_NAME}")
set(EXE_NAME "${TARGET_BASE_NAME}")

# Tell CMake to create the qt-hello executable
add_executable(${EXE_NAME} main.cpp)

# Add the Qt5Core for linking
target_link_libraries(${EXE_NAME} ${Qt5Core_LIBRARIES})

# Installation
install(TARGETS ${EXE_NAME} RUNTIME DESTINATION bin)
